import model.*;
import org.junit.*;
import static org.junit.Assert.*;

public class PawnTest{

    Pawn p;

    @Before
    public void setUp(){
      p = new Pawn(Type.WHITE, 1, null, 1, 2);
    }

    @After()
    public void tearDown(){
      p = null;
    }

    @Test
    public void testGetPosition(){
      assertEquals(p.getPosition()[0], 1); //x
      assertEquals(p.getPosition()[1], 2); //y
    }

    @Test
    public void testSetPosition(){
      p.setPosition(9,7);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetPositionExceptionX(){
      p.setPosition(-1,0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetPositionExceptionY(){
      p.setPosition(0,-1);
    }

}
