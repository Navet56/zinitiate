import model.*;
import org.junit.*;
import static org.junit.Assert.*;

public class GameEngineTest{

    GameEngine game;
    GameBoard gb;
    Player p1;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @Before
    public void setUp(){
      p1 = new Player(Type.BLACK, "p1", false);
      ArrayList<Pawn> pawns = new ArrayList<Pawn>();
      p = new Pawn(Type.BLACK, 1, p1, 1, 2);
      pawns.add(p);
      gb = new GameBoard(11, pawns);
      game = new GameEngine(p1, new Player(Type.WHITE, "p2", false), gb, Mode.DUO);
    }


    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @After()
    public void tearDown(){
      game = null;
    }

    @Test
    public void testGetCurrentPlayer(){
      assertEquals(p1, game.getCurrentPlayer());
    }

    @Test
    public void testCheckRules(){
      assertTrue(game.checkRules(2,2));
      assertFalse(game.checkRules(-2,2));
    }

    @Test
    public void testCheckWin(){
      assertTrue(game.checkWin()));//player 1 won because he has the only pawn in the game so necessarily his pawn is aligned with himself
    }

    @Test
    public void testMovePawn(){
      game.movePawn(1,2,-2);
      assertTrue(outContent.toString().contains("p")); // if console print something (I put p because in french and english, a pawn has a P)
      game.movePawn(1,2,2);
      assertFalse(outContent.toString().contains("p"));

    }

}
