import model.*;
import org.junit.*;
import static org.junit.Assert.*;

public class PlayerTest{

    Player p;

    @Before
    public void setUp(){
      p = new Player(Type.WHITE, "player", false);
    }

    @After()
    public void tearDown(){
      p = null;
    }

    @Test
    public void testGetType(){
      assertEquals(p.getType(), Type.WHITE);
    }

    @Test
    public void testGetName(){
      assertEquals(p.getName(), "player");
    }

    @Test
    public void testIsAutoPlayer(){
      assertEquals(p.isAutoPlayer(), false);
    }

}
