import model.*;
import org.junit.*;
import static org.junit.Assert.*;

public class GameBoardTest{

    GameBoard gb;
    Pawn p;

    @Before
    public void setUp(){
      ArrayList<Pawn> pawns = new ArrayList<Pawn>();
      p = new Pawn(1, Type.WHITE, 1, 2, new Player(Type.WHITE, "player 1", false));
      pawns.add(p);
      gb = new GameBoard(11, pawns);
    }

    @After()
    public void tearDown(){
      gb = null;
    }

    @Test
    public void testKill(){
      gb.kill(p);
      assertEquals("Removing 1 pawn from the pawns list", gb.getPawnsList(null).size(), 0);
    }

    @Test(expected = NullPointerException.class)
    public void testKillException(){
      gb.kill(p);
      gb.kill(p); //if the pawn is already killed, kill(Pawn) throws NullPointerException
    }

    @Test
    public void testGetCase(){
      assertEquals(gb.getCase(1,2), p);
      assertNull(gb.getCase(5,5));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetCaseExceptionX(){
      gb.getCase(-5,2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetCaseExceptionY(){
      gb.getCase(5,-1);
    }

    @Test
    public void testGetPawn(){
      assertEquals(gb.getPawn(1), p);
      assertNull(gb.getPawn(0));
    }

}
