import model.Model;
import control.Controller;
import model.Language;

import javax.sound.sampled.*;
import javax.swing.SwingUtilities;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Main class to launch the program
 * @author DIBERDER Evan
 */
public class Main {

  /**
   * Allows to initialize the program (the models, the view and the controller)
   * @param args the main args
   */
  public static void init(String[] args){
    Controller control = new Controller(new Model());
    Language.setLanguage('f');
    if(args.length > 0) {
      if (args[0].equals("-cli")){ control.showCLI();}
      else System.out.println("Erreur \"" + args[0] + "\" : Argument non valide \nle seul argument valide est -cli pour avoir Zen en mode console");
    } else control.showGUI();
  }

  /**
   * Main method
   * Uses SwingUtilities.invokeLater() to be thread safe
   * @param args the arguments that we can type during the program launch (<code>java Main arg1 arg2 ...</code>)
   */
  public static void main(String[] args) {
    SwingUtilities.invokeLater(() -> init(args));
  }

}
