package view.graphicalView;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * The Main Frame class
 */
public class MainFrame extends JFrame {

    private final EndPanel losePanel;
    public GamePanel gamePanel;
    private final MainMenu menu;
    private final MenuPanel menuPanel;
    private final EndPanel winPanel;

    /**
     * Main Frame constructor
     * @param title the Window title
     * @throws HeadlessException
     */
    public MainFrame(String title) throws HeadlessException {
        super(title);
        BorderLayout bl = new BorderLayout();
        bl.setHgap(20);
        bl.setVgap(20);
        this.setLayout(bl);
        this.setSize(1200,800);
        this.menu = new MainMenu();
        this.menuPanel = new MenuPanel();
        this.gamePanel = new GamePanel();
        this.winPanel = new EndPanel("data/win.png");
        this.losePanel = new EndPanel("data/lose.jpg");
        this.setJMenuBar(this.menu);
        this.add(this.menuPanel, BorderLayout.CENTER);
        this.setBackground(Color.GRAY);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public EndPanel getLosePanel() {
        return losePanel;
    }

    public EndPanel getWinPanel() {
        return winPanel;
    }

    public MenuPanel getMenuPanel () {
        return menuPanel;
    }

    public MainMenu getMenu() {
        return this.menu;
    }

    public GamePanel getMainPanel() {
        return this.gamePanel;
    }

    /**
     * allows to refresh the JFrame
     */
    public void update(){
        this.revalidate();
        this.menuPanel.update();
        this.setSize(1200,801);//the only way to really update ...
    }

}
