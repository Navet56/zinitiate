package view.graphicalView;

import model.Language;

import javax.swing.*;
import java.awt.*;

/**
 * Rules window with the App informations
 */
public class RulesFrame extends JFrame {

    private final JPanel panel;

    /**
     * RulesFrame constructor
     * @throws HeadlessException
     */
    public RulesFrame () throws HeadlessException {
        super("Règles du jeu");
        this.panel = new JPanel();
        this.panel.add(new JLabel(Language.rules));
        this.add(this.panel);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setSize(900,300);


    }


}
