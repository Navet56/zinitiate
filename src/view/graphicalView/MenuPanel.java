package view.graphicalView;

import model.Util;
import model.Language;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * The JPanel of the Main Menu of the game
 */
public class MenuPanel extends JPanel {

    private final JPanel buttonPanel;
    private final JButton startButton;
    private final JButton quitButton;
    private final JButton rulesButton;
    private final JButton newGameButton;
    private final JButton loadGameButton;

    /**
     * MenuPanel constructor
     */
    public MenuPanel() {
        this.setLayout(new GridLayout(1,2));
        this.buttonPanel = new JPanel(new GridLayout(3,1));
        this.buttonPanel.setBackground(Color.GRAY);
        this.startButton = Util.StylishButton(Language.guiStartGame);

        this.rulesButton = Util.StylishButton(Language.guiSeeRules);
        this.quitButton = Util.StylishButton(Language.guiQuit);
        this.newGameButton = Util.StylishButton(Language.newGame);
        this.loadGameButton = Util.StylishButton(Language.loadLast);

        this.startButton.setFont(new Font("Arial", Font.PLAIN, 30));
        this.rulesButton.setFont(new Font("Arial", Font.PLAIN, 30));
        this.quitButton.setFont(new Font("Arial", Font.PLAIN, 30));
        this.newGameButton.setFont(new Font("Arial", Font.PLAIN, 30));
        this.loadGameButton.setFont(new Font("Arial", Font.PLAIN, 22));

        this.buttonPanel.add(startButton);
        this.buttonPanel.add(rulesButton);
        this.buttonPanel.add(quitButton);
        this.add(new JLabel(new ImageIcon("data/background.jpg")));
        this.add(buttonPanel);


    }

    public JButton getStartButton() {
        return startButton;
    }

    public JButton getQuitButton() {
        return quitButton;
    }

    public JButton getRulesButton() {
        return rulesButton;
    }

    public JButton getNewGameButton() {
        return newGameButton;
    }

    public JButton getLoadGameButton() {
        return loadGameButton;
    }

    /**
     * Allows to change the JButtons to show the screen for choose new game of load game
     */
    public void goToGameChoice(){
        this.buttonPanel.removeAll();
        this.buttonPanel.add(this.newGameButton);
        this.buttonPanel.add(this.loadGameButton);
        this.update();
    }

    /**
     * Allows to refresh the panel
     */
    public void update(){
        this.revalidate();
        this.repaint();
    }


}
