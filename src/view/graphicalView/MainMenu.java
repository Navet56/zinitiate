package view.graphicalView;
import model.Language;

import javax.swing.*;

/**
 * Main JMenuBar class
 */
public class MainMenu extends JMenuBar{
    private JMenu file;
    private JMenu edit;
    private JMenu help;
    private final JMenuItem newFile;
    private final JMenuItem quitWithSaving;
    private final JMenuItem quitHard;
    private final JMenuItem about;
    private final JMenuItem rules;
    private final JMenuItem language;
    private final JMenuItem music;


    /**
     * MainMenu constructor
     */
    public MainMenu() {
        this.file = new JMenu(Language.file);
        this.edit = new JMenu(Language.edit);
        this.help = new JMenu(Language.help);
        this.newFile = new JMenuItem(Language.newGame);
        this.quitHard = new JMenuItem(Language.guiQuitHard);
        this.quitWithSaving = new JMenuItem(Language.guiQuitWithSaving);
        this.about = new JMenuItem(Language.about);
        this.rules = new JMenuItem(Language.rulesItem);
        this.language = new JMenuItem(Language.lang);
        this.music = new JMenuItem(Language.playStop);


        this.file.add(this.newFile);
        this.file.add(this.quitWithSaving);
        this.file.add(this.quitHard);
        this.add(this.file);
        this.edit.add(this.language);
        this.edit.add(this.music);
        this.add(this.edit);
        this.help.add(this.about);
        this.help.add(this.rules);
        this.add(this.help);
    }

    public JMenuItem getMusic() {
        return music;
    }

    public JMenuItem getRules() {
        return rules;
    }

    public JMenuItem getNewFile() {
        return newFile;
    }

    public JMenuItem getQuit() {
        return quitHard;
    }

    public JMenuItem getQuitSave() {
        return quitWithSaving;
    }

    public JMenuItem getAbout() {
        return about;
    }

    public JMenuItem getLanguage() {
        return this.language;
    }
}
