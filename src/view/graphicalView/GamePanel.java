package view.graphicalView;

import model.Util;
import model.Direction;
import model.Language;

import javax.swing.*;
import java.awt.*;

/**
 * Game JPanel (with the grid and the controls, the Main GUI in fact)
 * @see JPanel
 */
public class GamePanel extends JPanel {

    private final JLabel playerLabel;
    public final JButton verifBtn;
    public JButton moveBtnH ;
    public JButton moveBtnD;
    public JButton moveBtnB ;
    public JButton moveBtnG ;
    public JButton moveBtnA ;
    public JButton moveBtnC ;
    public JButton moveBtnE ;
    public JButton moveBtnF ;
    public JTextField pawnSelectField ;
    private final JPanel boardPanel;
    private final JLabel statusLabel;

    /**
     * GamePanel constructor
     */
    public GamePanel() {
        this.setLayout(new BorderLayout());
        BorderLayout bl = new BorderLayout();
        bl.setHgap(20);
        bl.setVgap(20);

        this.boardPanel = new JPanel(new BorderLayout());

        JPanel controlPanel = new JPanel(bl);
        JPanel movePanel = new JPanel(new GridLayout(3,3));

        this.moveBtnH = Util.ControlButton(Direction.N);
        this.moveBtnD = Util.ControlButton(Direction.E);
        this.moveBtnB = Util.ControlButton(Direction.S);
        this.moveBtnG = Util.ControlButton(Direction.W);
        this.moveBtnA = Util.ControlButton(Direction.NE);
        this.moveBtnC = Util.ControlButton(Direction.NW);
        this.moveBtnE = Util.ControlButton(Direction.SW);
        this.moveBtnF = Util.ControlButton(Direction.SE);
        this.pawnSelectField = new JTextField("0");
        this.pawnSelectField.setFont(new Font("Arial", Font.PLAIN, 30));

        movePanel.add(this.moveBtnC);
        movePanel.add(this.moveBtnH);
        movePanel.add(this.moveBtnA);
        movePanel.add(this.moveBtnG);
        movePanel.add(this.pawnSelectField);
        movePanel.add(this.moveBtnD);
        movePanel.add(this.moveBtnE);
        movePanel.add(this.moveBtnB);
        movePanel.add(this.moveBtnF);

        for (Component button : movePanel.getComponents()) {
            button.setPreferredSize(new Dimension(80, 80));
        }

        controlPanel.add(movePanel, BorderLayout.CENTER);
        this.verifBtn = Util.StylishButton(Language.guiCheckWin);
        controlPanel.add(verifBtn, BorderLayout.SOUTH);
        verifBtn.setPreferredSize(new Dimension(80, 80));
        JPanel statusPanel = new JPanel(new BorderLayout());
        this.playerLabel = new JLabel();
        this.statusLabel = new JLabel();
        JPanel helpPanel = new JPanel();
        helpPanel.setLayout(new BoxLayout(helpPanel, BoxLayout.Y_AXIS));
        helpPanel.add(new JLabel(Language.guiQuickRules1));
        helpPanel.add(new JLabel(Language.guiQuickRules2));
        helpPanel.add(new JLabel(Language.guiQuickRules3));
        helpPanel.add(new JLabel(Language.guiQuickRules4));

        statusPanel.add(this.playerLabel, BorderLayout.NORTH);
        statusPanel.add(this.statusLabel, BorderLayout.CENTER);
        statusPanel.add(helpPanel, BorderLayout.SOUTH);
        statusPanel.setPreferredSize(new Dimension(300, 300));
        controlPanel.add(statusPanel, BorderLayout.NORTH);
        this.add(controlPanel, BorderLayout.EAST);
        this.add(this.boardPanel, BorderLayout.CENTER);

    }

    public JLabel getPlayerLabel(){
        return this.playerLabel;
    }

    public JPanel getBoardPanel(){ return this.boardPanel;}

    public JLabel getStatusLabel () {
        return this.statusLabel;
    }

}
