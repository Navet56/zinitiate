package view.graphicalView;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.GridLayout;
import java.awt.HeadlessException;

/**
 * Classic window to show a text
 */
public class SimpleFrame extends JFrame {

    /**
     * SimpleFrame constructor
     * @param title
     * @param text
     * @param x x coord to set the size
     * @param y y coord to set the size
     * @throws HeadlessException
     */
    public SimpleFrame(String title, String text, int x, int y) throws HeadlessException {
        super(title);
        JPanel panel = new JPanel();
        JLabel label = new JLabel(text);
        label.setHorizontalAlignment(JLabel.CENTER);
        panel.add(label);
        this.add(panel);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setSize(x,y);


    }


}
