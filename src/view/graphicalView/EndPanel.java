package view.graphicalView;

import model.Util;
import model.Language;

import javax.swing.*;
import java.awt.*;

/**
 * EndPanel class (represents the end screen (win or lose))
 */
public class EndPanel extends JPanel {
    private final JButton returnButton;

    /**
     * EndPanel constructor
     * @param filePath the filePath of the image (lose or win)
     */
    public EndPanel(String filePath){
        this.returnButton = Util.StylishButton(Language.guiBackMenu);
        this.returnButton.setPreferredSize(new Dimension(200, 200));
        this.setLayout(new BorderLayout());
        this.add(new JLabel(new ImageIcon(filePath)),BorderLayout.CENTER);
        this.add(this.returnButton,BorderLayout.SOUTH);

    }

    public JButton getReturnButton() {
        return returnButton;
    }
}
