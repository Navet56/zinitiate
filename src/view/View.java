package view;

import model.Language;
import model.Model;
import view.graphicalView.MainFrame;
import view.graphicalView.MenuPanel;

import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Main View class
 */
public class View {

    private final Model model;
    private Clip clip;
    private MainFrame mainFrame;
    private JPanel gameboard;
    public MenuPanel menuPanel;
    private final ConsoleView cli;

    /**
     * View constructor
     * @param model the Model instance
     */
    public View(Model model) {
        this.model = model;
        this.cli = new ConsoleView(model);
        File file = new File("data/music.wav");
        try {
            this.clip = AudioSystem.getClip();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
        AudioInputStream ais = null;
        try {
            ais = AudioSystem.getAudioInputStream( file );
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            this.clip.open(ais);
        }catch (LineUnavailableException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.clip.loop(Clip.LOOP_CONTINUOUSLY);
    }

    /**
     * Allows to start or stop the music
     */
    public void playStopMusic(){
        if(this.clip.isActive())this.clip.stop();
        else this.clip.start();
    }
    /**
     * Allows to start the Main JFrame
     */
    public void startGUI(){
        this.mainFrame = new MainFrame(Model.TITLE);
    }

    /**
     * allows to init the CLI interface
     */
    public void startCLI() {
        this.cli.showConsole();
    }


    /**
     * main frame getter
     * @return the MainFrame instance
     */
    public MainFrame getMainFrame() {
        return this.mainFrame;
    }

    /**
     * Model getter
     * @return the Model instance
     */
    public Model getModel() {
        return this.model;
    }

    /**
     * allows to init the board (GUI)
     */
    public void initBoard () {
        int length = Model.BOARDLENGTH;
        this.gameboard = new JPanel(new GridLayout(length, length));
        this.gameboard.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        this.mainFrame.getMainPanel().getPlayerLabel().setText(Language.guiTurn + this.model.getGame().getCurrentPlayer().getName());

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                model.Pawn pawn = this.model.getGame().getBoard().getCase(i, j);
                String pawnStr = (pawn == null) ? "" : "" + pawn;
                JLabel label = new JLabel(pawnStr);
                if(pawn != null){
                    makePawnLabel(label, pawn.getNumber());
                }
                label.setName(""+i+","+j);
                label.setHorizontalAlignment(JLabel.CENTER);
                label.setBorder(BorderFactory.createLineBorder(Color.BLACK));
                this.gameboard.add(label);
            }
        }
        this.mainFrame.getMainPanel().getBoardPanel().add(this.gameboard, BorderLayout.CENTER);
    }

    /**
     * allows to update the pawn placement (GUI)
     */
    public void updatePlacementGUI () {
        for (model.Pawn p : this.model.getGame().getBoard().getPawnsList(null)) {
            if (p.isMoving()) {
                JLabel pawnLabel = getLabel(p.getNumber());
                if(pawnLabel != null){
                    cleanLabel(pawnLabel);
                    JLabel newLabel = getLabel(p.getPosition()[0], p.getPosition()[1]);
                    makePawnLabel(newLabel,p.getNumber());
                }
                p.setMoved(false);
            }
        }
    }

    /**
     * This method allows to clean a JLabel (for a pawn movement for exemple)
     * @param label the label to clean
     */
    private void cleanLabel(JLabel label){
        label.setText("");
        label.setIcon(null);
    }

    /**
     * Allows to paint a pawn in it new place
     * @param l the JLabel of the pawn
     * @param num the pawn's number
     */
    private void makePawnLabel(JLabel l, int num) {
        l.setText("" + num);
        if (num == 0){
            l.setForeground(Color.WHITE);
            l.setIcon(new ImageIcon("data/zenPawn.png"));
            l.setHorizontalTextPosition(JLabel.CENTER);
            l.setVerticalTextPosition(JLabel.CENTER);
        } else if(num > 12) {
            l.setForeground(Color.WHITE);
            l.setIcon(new ImageIcon("data/blackPawn.png"));
            l.setHorizontalTextPosition(JLabel.CENTER);
            l.setVerticalTextPosition(JLabel.CENTER);
        } else {
            l.setForeground(Color.BLACK);
            l.setIcon(new ImageIcon("data/whitePawn.png"));
            l.setHorizontalTextPosition(JLabel.CENTER);
            l.setVerticalTextPosition(JLabel.CENTER);
        }
    }

    /**
     * JLabel getter from x and y coordinates (in the grid)
     * @param x x coord
     * @param y y coord
     * @return the label of the pawn
     */
    private JLabel getLabel (int x, int y) {
        JLabel label = null;
        for (Component l : this.gameboard.getComponents()) {
            if (l instanceof JLabel) {
                JLabel la = (JLabel) l;
                if(la.getName().equals(""+x+","+y)){
                    return la;
                }
            }
        }
        return label;
    }

    /**
     * JLabel getter from the number of a pawn
     * @param num pawn number
     * @return the label of the pawn
     */
    private JLabel getLabel (int num) {
        JLabel label = null;
        for (Component l : this.gameboard.getComponents()) {
            if (l instanceof JLabel) {
                JLabel la = ( JLabel ) l;
                if (la.getText().equals("" + num)) {
                    return la;
                }
            }
        }
        return label;
    }

    /**
     * Allows to stop the music
     */
    public void stopMusic() {
        this.clip.stop();
    }
}
