package view;

import model.*;

import java.util.Scanner;

/**
 * The view for the CLI mode
 */
public class ConsoleView {

    private final Model model;

    /**
     * The ConsoleView constructor
     * @param model the Model instance
     */
    public ConsoleView(Model model){
        this.model = model;
    }

    /**
     * This method starts the console menu
     */
    public void showConsole(){
        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n" +
                "@@@@@@@@@@@@@@@@@@@@@@@@*                       *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n" +
                "@@@@@@@@@@@@@@@@@@*          *@@@@@@@@@@@@@@*            @@@@@@@@@@@@@@@@@@@@@@@\n" +
                "@@@@@@@@@@@@@@@   ,@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@       @@@@@@@@@@@@@@@@@@@@@\n" +
                "@@@@@@@@@@@@    @@@@@@@@@@@@@@@                   @@@@@@@@      @@@@@@@@@@@@@@@@\n" +
                "@@@@@@@@@@   .@@@@@@@@@         @@@@@@@@@@@@@@@@@.    @@@@@@@@@    @@@@@@@@@@@@@\n" +
                "@@@@@@@@@   @@@@@@@     @@@@@@@@@@@@@@@@@@@@@@     @@@@@@@@@@@@@@@    @@@@@@@@@@\n" +
                "@@@@@@@@   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    %@@@@@@@@@@@@@@@@@@@@@   @@@@@@@@\n" +
                "@@@@@@   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@/    @@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@\n" +
                "@@@@@   @@@@@@@@@@@@@@@@@@@@@@@@@@@@.    @@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@   @@@@\n" +
                "@@@@   @@@@@@@@@@@@@@@@@@@@@@@@@@    %@@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@   @@@\n" +
                "@@@   @@@@@@@@@@@@@@@@@@@@@@@@@   /@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  %@@@@@   @@\n" +
                "@@@  @@@@@@@@@@@@@@@@@@@@@@@%   @@@@@@@@             @    @@@@@@@@@   @@@@@.  @@\n" +
                "@@   @@@@@@@@@@@@@@@@@@@     @@@@@@@@    @@@@@@@@@@@@       (@@@@@@   @@@@@   @@\n" +
                "@@   @@@@@@@@@@@@@@@@@@    @@@@@@@@@@  @@@@@@@@@@@@@@   @@@*   @@@@   @@@@@   @@\n" +
                "@@   @@@@@@@@@@@@@@@@       @@@@@@@@@@             @@   @@@@@@   @@@  @@@@@    @\n" +
                "@@@   @@@@@@@@@@@@@@      @@@@@@@@@@@@  @@@@@@@@@@@@@@   @@@@@@@@   @  &@@@   @@\n" +
                "@@@   @@@@@@@@@@@@      @@@@@@@@@@@@@@  /@@@@@@@@@@@@@   @@@@@@@@@@    @@@@   @@\n" +
                "@@@   @@@@@@@@@@      @@@@@@@@@@@@@@@               @   @@@@@@@@@@@@@@@@@@   @@@\n" +
                "@@@   @@@@@@@@        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  @@@@@@@@@@@@@@@@@   ,@@@\n" +
                "@@@@  @@@@@@@       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    @@@@\n" +
                "@@@@   @@@@@                                        @@@@@@@@@ @@@@@@@@@@   @@@@@\n" +
                "@@@@@   @@@@@@@@@@@@@@@@@@@@@@@@  @@@@@@@@@@@@@@@@@@@@@@          @@@@@    @@@@@\n" +
                "@@@@@@   .@@@@@@@@   @@@@   *@@@  @@@   @         @@  @@@    @@@@@@@@@   @@@@@@@\n" +
                "@@@@@@@@   .@@@@@@@  .@@&      #   @@  @@@%   @@@@@   @@*         @@    @@@@@@@@\n" +
                "@@@@@@@@@@     @@@@   @@%  *@@    @@@  /@@@   @@@@@  #@@    @@@@%    &@@@@@@@@@@\n" +
                "@@@@@@@@@@@@@@    @@@@@@@&@@@@@@&@@@@   @@@  (@@@@@ (@@@@@.      #@@@@@@@@@@@@@@\n" +
                "@@@@@@@@@@@@@@@@@      (@@@@@@@@@@@@@@@@@@@@@@@@@@@@(      &@@@@@@@@@@@@@@@@@@@@\n" +
                "@@@@@@@@@@@@@@@@@@@@@@.                              .@@@@@@@@@@@@@@@@@@@@@@@@@@");

        Scanner in;
        char status = ' ';

        while (status != 'q') {
            System.out.println("                    ***********************************");
            System.out.println("                    *  " + Language.welcome +" !   *");
            System.out.println("                    *     "+ Language.menuStartGame + "  *");
            System.out.println("                    *     "+ Language.menuHelp+"      *");
            System.out.println("                    *     "+ Language.menuLanguage +"    *");
            System.out.println("                    ***********************************");
            in = new Scanner(System.in);
            status = in.nextLine().charAt(0);
            switch (status) {
                case 'q' :
                    System.out.println( Language.cliQuit +" o/n");
                    status = input();
                    if(status == 'o'){
                        status = 'q';
                        break;
                    }
                    else continue;
                case 's':
                    startGameLoop();
                    break;
                case 'a' :
                    help();
                    break;
                case 'l':
                    System.out.println(Language.changeLang);
                    Language.setLanguage((Language.language=='f')?'a':'f');
                    break;
                default:
                    System.out.println(Language.clinotOption);
                    break;
            }
        }
    }

    /**
     * This method allows to make an input without errors
     * @return the new inpout character
     */
    private char input(){
        boolean win = false;
        char d = 'a';
        while(!win) {
            try {
                d = (new Scanner(System.in)).nextLine().charAt(0);
                if(d != 'a' && d != 'b' && d != 'c' && d != 'd' && d != 'e' && d != 'f' && d != 'g' && d != 'h' && d != 's' && d != 'q' && d != 'n' && d !='o') throw new IllegalArgumentException();
                win = true;
            } catch (Exception e) {
                System.out.println(Language.wrongInput);
            }
        }
        return d;
    }

    /**
     * The game loop (in CLI mode)
     */
    private void startGameLoop(){
        Scanner in;

        System.out.println(Language.loadLast +" ? (o/n)");
        char answer = input();
        this.model.initGame((answer != 'o'), 'c');
        GameEngine gameEngine = this.model.getGame();

        char status = ' ';
        String playerName;

        while (status != 'q') {
            System.out.println(gameEngine.getCurrentPlayer().getName());
            playerName = gameEngine.getCurrentPlayer().getName();
            gameEngine.getBoard().update('c');
            System.out.println(playerName + Language.cliWhatDo);
            if(gameEngine.getCurrentPlayer().isAutoPlayer()){
                gameEngine.movePawnIA();
            }
            else {
                System.out.println(Language.cliMovePawn);
                System.out.println(Language.cliCheckWin);
                System.out.println(Language.cliForQuit);
                status = input();
                switch (status) {
                    case 'q':
                        System.out.println( Language.cliQuitWithoutSave +" ? o/n");
                        status = input();
                        if (status == 'o') {
                            this.model.saveGame();
                            System.out.println(Language.wellSaved);
                        }
                        status = 'q';
                        break;
                    case 'd':
                        System.out.print(Language.whatPawn);
                        in = new Scanner(System.in);
                        int p = in.nextInt();
                        System.out.println(Language.cliMoves);
                        char d = input();

                        Direction dir = null;
                        switch (d) {
                            case 'h':
                                dir = Direction.N;
                                break;
                            case 'd':
                                dir = Direction.E;
                                break;
                            case 'b':
                                dir = Direction.S;
                                break;
                            case 'g':
                                dir = Direction.W;
                                break;
                            case 'a':
                                dir = Direction.NE;
                                break;
                            case 'c':
                                dir = Direction.NW;
                                break;
                            case 'e':
                                dir = Direction.SW;
                                break;
                            case 'f':
                                dir = Direction.SE;
                                break;
                            default:
                                System.out.println("Veuillez entrer uniquement 1 de ces 8 caractères");
                        }
                        int move = gameEngine.movePawn(p, dir, true);
                        System.out.println(Util.getStatusString(move));
                        break;
                    case 'c':
                        if (gameEngine.checkWin()){
                            System.out.println("           " + playerName + Language.won);
                            System.out.println("♪ღ♪*•.¸¸.•*¨¨*•.♪ღ♪*•.¸¸.•*¨¨*•.♪ღ♪");
                            System.out.println(Language.congrats);
                            System.out.println("♪ღ♪*•.¸¸.•*¨¨*•.♪ღ♪*•.¸¸.•*¨¨*•.♪ღ♪");
                            status = 'q';
                        } else
                            System.out.println(Language.notWon);
                        break;
                    default:
                        System.out.println(Language.wrongInput);
                        break;
                }
            }
        }
    }

    /**
     * Shows the rules and the about informations
     */
    private void help(){
        System.out.println(Language.rules);

        System.out.println("\n               ********************************************");
        System.out.println("               *              " + Model.TITLE + "                *");
        System.out.println("               *       Développé par " + Model.AUTHOR +"              *");
        System.out.println("               *              Version " + Model.VERSION +"                 *");
        System.out.println("               ********************************************");
    }

}
