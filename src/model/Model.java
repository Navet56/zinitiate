package model;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * General model class
 * allows to init the game (from a saving file or not)
 */
public class Model {
    public static final String VERSION = "1.1";
    public static final String TITLE = "Zen l'Initié";
    public static final String AUTHOR = "Navet56";
    public static final String LICENSE = "GNU GPL v3";
    public static final int BOARDLENGTH = 11;

    private GameEngine game;

    public GameEngine getGame(){ return this.game;}


    /**
     * allows to init the game
     * @param newGame if newGame is false, this method will load the saving file to continue the last gaming session
     * @param c game mode 'g' for GUI and 'c' for CLI
     */
    public void initGame(boolean newGame, char c){

        ArrayList<Pawn> pawns = new ArrayList<>();
        String name1, name2;

        name1 = "White";
        name2 = "Black";

        Player p1 = new Player(Type.WHITE, name1, false);
        Player p2 = new Player(Type.BLACK, name2, false);
        Player pz = new Player(Type.ZEN, "Zen", false);

        //Initializes the pawns
        pawns.add(new Pawn(0 , 5 , 5, pz));
        pawns.add(new Pawn(1, 5 , 0, p1));
        pawns.add(new Pawn(2, 3 , 2, p1));
        pawns.add(new Pawn(3, 1 , 4, p1));
        pawns.add(new Pawn(4, 1 , 6, p1));
        pawns.add(new Pawn(5, 3 , 8, p1));
        pawns.add(new Pawn(6, 5 , 10, p1));
        pawns.add(new Pawn(7, 7 , 2, p1));
        pawns.add(new Pawn(8, 9 , 4, p1));
        pawns.add(new Pawn(9, 9 , 6, p1));
        pawns.add(new Pawn(10, 7 , 8, p1));
        pawns.add(new Pawn(11, 10 , 10, p1));
        pawns.add(new Pawn(12, 0 , 0, p1));
        pawns.add(new Pawn(13, 4 , 1, p2));
        pawns.add(new Pawn(14, 2 , 3, p2));
        pawns.add(new Pawn(15, 0 , 5, p2));
        pawns.add(new Pawn(16, 4 , 9, p2));
        pawns.add(new Pawn(17, 2 , 7, p2));
        pawns.add(new Pawn(18, 6 , 1, p2));
        pawns.add(new Pawn(19, 8 , 3, p2));
        pawns.add(new Pawn(20, 10 , 5, p2));
        pawns.add(new Pawn(21, 8 , 7, p2));
        pawns.add(new Pawn(22, 10 , 0, p2));
        pawns.add(new Pawn(23, 0 , 10, p2));
        pawns.add(new Pawn(24, 6 , 9, p2));

        GameBoard gb;
        if(newGame) gb = new GameBoard(BOARDLENGTH, pawns);
        else gb = new GameBoard(BOARDLENGTH, Util.readZSAVEFile(), p1, p2, pz);

        Mode mode = Mode.SOLO;
        if(c == 'c') {
            System.out.println( Language.soloOrDuo + " (o/n)");
            if((new Scanner(System.in).nextLine().charAt(0) == 'o')) mode = Mode.DUO;
        } else {
            int a = JOptionPane.showConfirmDialog(new JFrame(),Language.soloOrDuo);
            if(a == JOptionPane.YES_OPTION){
                mode = Mode.DUO;
            } else if( a == JOptionPane.CANCEL_OPTION){
                System.exit(0);
            }

        }

        if(mode == Mode.SOLO) p2.setAuto(true);

        this.game = new GameEngine(p1, p2, gb, mode);
    }


    public void saveGame() {
        String grid = this.game.getBoard().getGridString();
        try {
            Util.writeZSAVEFile(grid);
        } catch (FileNotFoundException e) {
            System.out.println("Error: unable to write to the backup file");
        }
    }

}
