package model;
/**
 * Direction enumeration
 * used to specify a cardinal point of a pawn movement
 * A pawn can be moved to :
 * <ul>
 *     <li>North (N)</li>
 *     <li>North East (NE)</li>
 *     <li>East (E)</li>
 *     <li>South East (SE)</li>
 *     <li>South (S)</li>
 *     <li>South West (SW)</li>
 *     <li>West (W)</li>
 *     <li>North West (NW)</li>
 * </ul>
 */
public enum Direction {
    N,
    NE,
    E,
    SE,
    S,
    SW,
    W,
    NW
}
