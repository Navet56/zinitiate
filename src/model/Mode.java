package model;
/**
 * 2 mode in the game :
 * Solo (Human vs Ai)
 * or Duo (Human vs Human)
 */
public enum Mode {
    SOLO,
    DUO
}
