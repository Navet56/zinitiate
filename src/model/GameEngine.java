package model;

import java.util.Random;

/**
 * The Game class
 * this class helps make the game work and follow the rules
 */
public class GameEngine {
    private final Mode mode;
    private final GameBoard board;
    private final Player[] players;
    private boolean currentPlayer;

    /**
     * GameEngine constructor
     * @param player1
     * @param player2
     * @param board the game board
     * @param mode the game mode (solo or duo)
     */
    public GameEngine(Player player1, Player player2, GameBoard board, Mode mode) {
        this.players = new Player[]{player1, player2};
        this.board = board;
        this.currentPlayer = false;
        this.mode = mode;
    }

    /**
     * @return the current Player object
     */
    public Player getCurrentPlayer(){ return this.currentPlayer ? this.players[1] : this.players[0];}

    /**
     * allows to set a a new position
     * @param p pawn's number
     * @param x new x coord
     * @param y new y coord
     * @param realMove do not kill pawns if this is a test move and not a effective move
     * @return
     */
    private boolean setNewPawnPos(int p, int x, int y, boolean realMove){
        boolean succeed = false;
        Pawn oldPawn = this.board.getPawn(x,y);
        if (oldPawn != null) {
            if(oldPawn.getPlayer() != this.getCurrentPlayer() && !realMove){
                this.board.kill(oldPawn);
            }
        }
        try {
            this.board.getPawn(p).setPosition(x, y);
            succeed = true;
        } catch(IllegalStateException e){
            e.getMessage();
            succeed = false;
        }
        return succeed;
    }

    /**
     * allows to make a pawn movement automatically (in Solo mode)
     * @return true if the move have been well done
     */
    public boolean movePawnRandom(){
        int p = (int) ( Math.random() * (24 - 13)) + 13;
        Direction dir = Direction.values()[new Random().nextInt(Direction.values().length)];
        return (movePawn(p, dir, true) > 0);
    }

    /**
     * allows to make a pawn movement automatically (in Solo mode)
     * but the move is more smart than random move
     * @return true if the move have been well done
     */
    public boolean movePawnIA(){
        int pawn = 0;
        int p = (int) ( Math.random() * (24 - 13)) + 13;
        Direction dir;
        int max = 0;
        Direction dire = Direction.values()[new Random().nextInt(Direction.values().length)];;
        for (int i = 0; i < 10; i++) {
            dir = Direction.values()[new Random().nextInt(Direction.values().length)];
            movePawn(p, dir, false);
            int goodMove = moveScore(p);
            if(goodMove > max){
                pawn = p;
                dire = dir;
                max = goodMove;
            }
            movePawn(pawn, Util.getOppositeDir().get(dir), false);
        }
        return (movePawn(pawn, dire, true) > 0);

    }

    /**
     * For IA
     * Allows to count the score of a move
     * @param num pawn's number
     * @return the score
     */
    private int moveScore(int num) {
        Pawn p = this.board.getPawn(num);
        int ret = 0;
        if(p != null) {
            int x = p.getPosition()[0];
            int y = p.getPosition()[1];

            if (x < 9 && x > 2 && y > 9 && y < 2) ret++;
            if (p.getPosBefore()[0] > 9 || p.getPosBefore()[1] > 9) ret++;
            if (p.getPosBefore()[0] < 6 && p.getPosBefore()[0] > 2 && p.getPosBefore()[1] < 6 && p.getPosBefore()[1] > 2)
                ret--;

            Pawn pawn;
            for (int i = -1; i <= 1; i++){
                for (int j = -1; j <= 1; j++) {
                    if(!(i == 0 && j == 0)){
                        pawn = this.board.getCase(correctPos(x+i),correctPos(y+j));
                        if((pawn != null && isPlayerCompatible(pawn)))
                            ret++;
                    }
                }
            }
        }
        return ret;
    }

    /**
     * to moving a pawn in the grid
     * @param num pawn number
     * @param dir direction of the move (Direction enum)
     * @return 1 if the move have been well done, -1 if the pawn can't be moved and -2 if this move don't repsect the rules
     */
    public int movePawn(int num, Direction dir, boolean realMove) {
        Pawn pawn = this.board.getPawn(num);
        boolean moved = false;
        boolean checkingRules = false;
        if (pawn != null && isPlayerCompatible(pawn)) {
            int x = pawn.getPosition()[0];
            int y = pawn.getPosition()[1];
            switch (dir) {
                case N: //Haut
                    x -= this.board.countColPawn(y);
                    if (!checkRules(pawn, dir, x, y)) break;
                    moved = setNewPawnPos(num, x, y, realMove);
                    checkingRules = true;
                    break;
                case S: //Bas
                    x += this.board.countColPawn(y);
                    if (!checkRules(pawn, dir, x, y)) break;
                    moved = setNewPawnPos(num, x, y, realMove);
                    checkingRules = true;
                    break;
                case W://Gauche
                    y -= this.board.countRowPawn(x);
                    if (!checkRules(pawn, dir, x, y)) break;
                    moved = setNewPawnPos(num, x, y, realMove);
                    checkingRules = true;
                    break;
                case E://Droite
                    y += this.board.countRowPawn(x);
                    if (!checkRules(pawn, dir, x, y)) break;
                    moved = setNewPawnPos(num, x, y, realMove);
                    checkingRules = true;
                    break;
                case NE://Diagonale nord est
                    x -= this.board.countDiagRightPawn(pawn);
                    y += this.board.countDiagRightPawn(pawn);
                    if (!checkRules(pawn, dir, x, y)) break;
                    moved = setNewPawnPos(num, x, y, realMove);
                    checkingRules = true;
                    break;

                case NW://Diagonale nord ouest
                    x -= this.board.countDiagLeftPawn(pawn);
                    y -= this.board.countDiagLeftPawn(pawn);
                    if (!checkRules(pawn, dir, x, y)) break;
                    moved = setNewPawnPos(num, x, y, realMove);
                    checkingRules = true;
                    break;
                case SW://Diagonale sud ouest
                    x += this.board.countDiagRightPawn(pawn);
                    y -= this.board.countDiagRightPawn(pawn);
                    if (!checkRules(pawn, dir, x, y)) break;
                    moved = setNewPawnPos(num, x, y, realMove);
                    checkingRules = true;
                    break;
                case SE://Diagonale sud est
                    x += this.board.countDiagLeftPawn(pawn);
                    y += this.board.countDiagLeftPawn(pawn);
                    if (!checkRules(pawn, dir, x, y)) break;
                    moved = setNewPawnPos(num, x, y, realMove);
                    checkingRules = true;
                    break;
                default:
                    System.out.println(Language.wrongInput);
            }
            if (moved) this.currentPlayer = !this.currentPlayer;
            if (!checkingRules) {
                return -2;
            }
        } else {
            return -1;
        }
        return 1;
    }

    /**
     * @return true if this action respects the rules
     */
    public boolean checkRules(Pawn p, Direction dir, int finalX, int finalY){
        boolean check = false;
        if(finalX < 0 || finalX >= this.board.getLength() || finalY < 0 || finalY >= this.board.getLength() || p == null || dir == null){
            return false;
        }
        if(this.board.getCase(finalX, finalY) != null){
            if(this.board.getCase(finalX, finalY).getPlayer() == this.getCurrentPlayer()){
                return false;
            }
        }
        if(p.getType() == Type.ZEN){
            Pawn futurePawn = new Pawn(-1,finalX,finalY,new Player(Type.ZEN, "Zen", false));
            if(!hasNeighbor(futurePawn)) return false;
        }
        if(!enemyOnTheWay(p,dir,finalX,finalY)){
            check = true;
        }
        return check;
    }

    /**
     * @return true if the current player won
     */
    public boolean checkWin() {
        int nbGood = 0;
        for (Pawn pawn : this.board.getPawnsList(getCurrentPlayer()))
            if (hasNeighbor(pawn)) nbGood++;

            return (nbGood == this.board.getPawnsList(getCurrentPlayer()).size());
    }



    /**
     * to see if a pawn is compatible with the current player
     * @param pawn the pawn to see
     * @return true if this pawn belongs to the current player, or if it's the Zen pawn of it's null
     */
    private boolean isPlayerCompatible(Pawn pawn){
        return (pawn == null) || (this.getCurrentPlayer() == pawn.getPlayer()) || (pawn.getType() == Type.ZEN);
    }

    /**
     * Is there enemies on the pawn way ?
     * @param p the pawn object
     * @param d the pawn direction
     * @param finalX the pawn x position (at the end)
     * @param finalY the pawn y position (at the end)
     * @return true if there are enemy on the pawn movement direction
     */
    private boolean enemyOnTheWay(Pawn p, Direction d, int finalX, int finalY){
        boolean ret = false;
        int x = p.getPosition()[0];
        int y = p.getPosition()[1];

        switch (d) {
            case N:
                for (int i = x; i >= finalX ; i--)
                    if (!isPlayerCompatible(this.board.getCase(i,y))){
                        ret = true;
                    }
                break;
            case S:
                for (int i = x; i < finalX ; i++) {
                    if (!isPlayerCompatible(this.board.getCase(i, y))) {
                        ret = true;
                    }
                }
                break;
            case W:
                for (int i = y; i >= finalY ; i--)
                    if (!isPlayerCompatible(this.board.getCase(x,i))) {
                        ret = true;
                    }
                break;

            case E:
                for (int i = y; i < finalY ; i++) {
                    if (!isPlayerCompatible(this.board.getCase(x, i))) {
                        ret = true;
                    }
                }
                break;

            case NW:
                int j = x;
                for (int i = y; i >= finalY ; i--) {
                    if (!isPlayerCompatible(this.board.getCase(j, i)))
                        ret = true;
                    if (j < finalX) break;
                    j--;
                }
                break;
            case NE:
                j = x;
                for (int i = y; i >= finalY ; i--) {
                    if (!isPlayerCompatible(this.board.getCase(j, i))) {
                        ret = true;
                        System.out.println(this.board.getCase(j, i));
                    }

                    if (j >= finalX) break;
                    j++;
                }
                break;
            case SW:
                j = x;
                for (int i = y; i < finalY ; i++) {
                    if (!isPlayerCompatible(this.board.getCase(j, i)))
                        ret = true;
                    if (j < finalX) break;
                    j--;
                }
                break;
            case SE:
                j = x;
                for (int i = y; i < finalY ; i++) {
                    if (!isPlayerCompatible(this.board.getCase(j, i)))
                       ret = true;
                    if (j >= finalX) break;
                    j++;
                }
                break;
        }
        return ret;
    }

    /**
     * allows to return a position in the grid (so not -1 etc)
     * @param pos coordinate
     * @return the coordinate without out ouf range (so -1 become 0 for example)
     */
    public int correctPos(int pos){
        int ret = pos;
        if(ret < 0){
            ret = 0;
        } else if(ret >= this.board.getLength()){
            ret = this.board.getLength()-1;
        }
        return ret;
    }

    /**
     * @param p the pawn
     * @return true if this pawn has a neighbor
     */
    private boolean hasNeighbor(Pawn p){
        boolean ret = false;
        int x = p.getPosition()[0];
        int y = p.getPosition()[1];
        Pawn pawn;
        for (int i = -1; i <= 1; i++){
            for (int j = -1; j <= 1; j++) {
                if(!(i == 0 && j == 0)){
                    pawn = this.board.getCase(correctPos(x+i),correctPos(y+j));
                    ret = (ret || pawn != null && isPlayerCompatible(pawn));
                }

            }
        }
        return ret;

    }

    public GameBoard getBoard() {
        return this.board;
    }


    public Mode getMode() {
        return this.mode;
    }
}
