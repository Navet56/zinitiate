package model;
/**
 * Model a player
 * it can be a human player or not
 */
public class Player {
    private final Type type;
    private final String name;
    private boolean auto;

    /**
     * Player contructor
     * @param type the colour of this Player (WHITE BLACK or ZEN)
     * @param name player name
     * @param auto is an computer player or human player
     */
    public Player(Type type, String name, boolean auto) {
        this.type = type;
        this.name = name;
        this.auto = auto;
    }

    public Type getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public boolean isAutoPlayer() {
        return auto;
    }

    public void setAuto(boolean b) {
        this.auto = b;
    }
}
