package model;
/**
 * Model a pawn with its position, its type and its number
 */
public class Pawn {
    private final Type type;
    private final int number;
    private final Player player;
    private boolean moved;
    private final int[] position;
    private int[] posBefore;

    /**
     * Pawn constructor
     * @param number pawn number
     * @param x x coord
     * @param y y coord
     * @param player the Player of the pawn
     */
    public Pawn(int number, int x, int y, Player player) {
        this.type = player.getType();
        this.number = number;
        this.player = player;
        this.position = new int[]{x, y};
        this.posBefore = new int[]{x,y};
    }

    public Type getType() {
        return type;
    }

    public int getNumber() {
        return number;
    }

    public Player getPlayer() {
        return player;
    }

    public int[] getPosition() {
        return position;
    }

    /**
     * to set the position of the pawn
     * @param x x coordinate
     * @param y y coordinate
     * @throws IllegalStateException
     */
    public void setPosition(int x, int y) throws IllegalStateException{
        if (x < 0 || y < 0) {
            throw new IllegalStateException("Set this pawn here is illegal " + x + "," + y);
        } else {
            this.moved = true;
            this.posBefore = new int[]{this.position[0], this.position[1]};
            this.position[0] = x;
            this.position[1] = y;
        }
    }

    /**
     * to get the color of the pawn
     * @return
     */
    public String getColorStr(){
        switch(this.type){
            case WHITE:
                return "\u001B[37m";
            case BLACK:
                return "\u001B[34m";
            case ZEN:
                return "\u001B[35m";
            default:
                return "\u001B[0m" ;
        }
    }

    /**
     * moved getter
     * @return true if this pawn is moving
     */
    public boolean isMoving() {
        return this.moved;
    }

    @Override
    public String toString(){ return ""+this.number;}


    /**
     * moved setter
     * @param moving
     */
    public void setMoved(boolean moving) {
        this.moved = moving;
    }

    /**
     * to get the previous position
     * @return
     */
    public int[] getPosBefore() {
        return posBefore;
    }


}
