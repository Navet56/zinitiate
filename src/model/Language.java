package model;

/**
 * Language static class allows to regroup all the text and change the language easly
 */
public class Language {

    public static String menuStartGame;
    public static String menuHelp;
    public static String menuLanguage;
    public static String cliQuit;
    public static String guiQuit;
    public static String clinotOption;
    public static String wrongInput;
    public static String loadLast;
    public static String cliWhatDo;
    public static String cliMovePawn;
    public static String cliCheckWin;
    public static String cliQuitWithoutSave;
    public static String cliForQuit;
    public static String wellSaved;
    public static String cliMoves;
    public static String whatPawn;
    public static String congrats;
    public static String won;
    public static String notWon;
    public static String rules;
    public static String soloOrDuo;
    public static String guiBackMenu;
    public static String guiTurn;
    public static String guiQuickRules1;
    public static String guiQuickRules2;
    public static String guiQuickRules3;
    public static String guiQuickRules4;
    public static String guiStartGame;
    public static String guiSeeRules;
    public static String guiQuitHard;
    public static String newGame;
    public static char language = 'f';
    public static String welcome;
    public static String changeLang;
    public static String lang;
    public static String file;
    public static String edit;
    public static String help;
    public static String about;
    public static String rulesItem;
    public static String guiCheckWin;
    public static String badMove;
    public static String goodMove;
    public static String badPawn;
    public static String guiQuitWithSaving;
    public static String playStop;
    public static String easyOrHard;

    /**
     * This static method allows to set the current language
     * @param l 'f' for French, 'a' for English
     */
    public static void setLanguage(char l){
        language = l;
        changeLanguage(l);
    }

    /**
     * Allows to change the langage
     * @param l 'f' for French, 'a' for English
     */
    private static void changeLanguage(char l){
        if(l=='f'){
            welcome = "Bienvenue sur " + Model.TITLE;
            clinotOption = "Ce n'est pas une option possible. Veuillez reessayer.";
            wrongInput = "Mauvaise saisie";
            lang = "Passer en anglais";
            playStop = "Jouer/arreter la musique";
            menuStartGame = "s pour démarrer une partie";
            menuHelp = "a pour voir les règles";
            menuLanguage = "l pour changer de langue";
            loadLast = "Charger la dernière sauvegarde";
            cliWhatDo = ", Que veux tu faire ?";
            cliMovePawn = "d pour déplacer un pion";
            cliCheckWin = "c pour check si tu as gagné";
            cliForQuit = "q pour quitter";
            badMove = "Impossible : règles non respectées";
            goodMove = "Pion déplacé avec succès !";
            badPawn = "Vous ne pouvez pas déplacer ce pion";
            help = "Aide";
            about = "A propos";
            rulesItem = "Règles";
            cliQuitWithoutSave = "Voulez vous quitter en sauvegardant";
            wellSaved = "Jeu sauvegardé avec succès, vous pourrez le reprendre plus tard";
            cliMoves = "h : déplacement vers le haut\n" +
                    "d : déplacement vers la droite\n" +
                    "b : déplacement vers le bas\n" +
                    "g : déplacement vers la gauche\n" +
                    "a : déplacement diagonale / vers le haut\n" +
                    "c : déplacement diagonale \\\\ vers le haut\n" +
                    "e : déplacement diagonale / vers le bas\n" +
                    "f : déplacement diagonale \\\\ vers le bas";
            whatPawn = "Déplacer quel pion ? ";
            congrats = "    B R A V O   A   T O I   !!!!";
            won = " a gagné !";
            file = "Fichier";
            edit = "Edition";
            notWon = "Tu n'as pas gagné, continuons.";
            guiCheckWin = "Verifier si gagné";
            rules = "<html><br>" +
                    "********* Aide du jeu Zen l'initié *********** \n         <br>" +
             " Le but du jeu est que tout les pions du joueur se touchent (sur le coté ou en diagonale)\n         <br>" +
             " Vous avez le droit de déplacer le pion de votre choix dans l'une des 8 diréctions\n         <br>" +
             " Le pion Zen (0) peut être déplacé par les 2 joueurs\n         <br>" +
             " Vous pouvez manger un pion adverse si vous arrivez sur sa case\n         <br>" +
             " Un déplacement se fait selon le nombre de pion sur la ligne du pion\n         <br>" +
             " Par exemple si je veux déplacer mon pion 3 vers le haut, et que sur la colonne de mon pion il y a en tout 3 pions\n         <br>" +
             " Alors mon pion va se déplacer de 3 cases vers le haut\n         <br>" +
             " Le pion Zen ne peux se déplacer que s'il arrive proche d'un autre pion.\n         <br>" +
             " Plus d'informations dans le manuel utilisateur.\n         <br>";
            soloOrDuo = "Voulez vous jouer en duo ? (2 joueurs humains)";
            guiBackMenu = "Revenir au menu principal";
            guiTurn = "C'est au tour de ";
            guiQuickRules1 = "Choisissez le numéro d'un pion";
            guiQuickRules2 = "Puis selectionnez la direction";
            guiQuickRules3 = "Et si cela respecte les règles,";
            guiQuickRules4 = "alors le pion sera déplacé";
            guiStartGame = "Demarrer une partie";
            guiSeeRules = "Voir les règles";
            guiQuitHard = "Quitter sans sauvegarder";
            guiQuitWithSaving = "Quitter en sauvegardant";
            guiQuit = "Quitter";
            newGame = "Nouvelle partie";
            changeLang = "Changement de la langue en cours ...\n" +
                    "C'est bon !";
        } else {
            welcome = "Welcome to " + Model.TITLE;
            clinotOption = "This is not an option. Please try again.";
            wrongInput = "Wrong entry";
            menuStartGame = "s to start a game";
            menuHelp = "a to see the rules";
            menuLanguage = "l to change the language";
            loadLast = "Load the last backup";
            cliWhatDo = ", What do you want to do?";
            cliMovePawn = "d to move a pawn";
            cliCheckWin = "c to check if you have won";
            cliForQuit = "q to quit";
            file = "File";
            help = "Help";
            about = "About";
            playStop = "Play/stop music";
            badMove = "Bad action : rules not respected";
            goodMove = "Pawn successfully moved!";
            badPawn = "Bad action : You can't move this pawn";
            rulesItem = "Rules";
            cliQuitWithoutSave = "Do you want to quit while saving";
            wellSaved = "Game saved successfully, you can resume it later";
            cliMoves = "h: move up \n" +
                    "d: move to the right \n" +
                    "b: move down \n" +
                    "g: move to the left \n" +
                    "a: move to north east \n" +
                    "c: move to north west \n" +
                    "e: move to south west \n" +
                    "f: move to south east\\\\ down";
            whatPawn = "Move which pawn?";
            congrats = "CONGRATULATION !!!!";
            won = "won!";
            guiQuit = "Quit";
            guiCheckWin = "Check if I win";
            notWon = "You didn't win, let's continue.";
            rules = "<html> <br>" +
                    "********* Help for the initiate Zen game *********** \n <br>" +
                    "The object of the game is for all of the player's pawns to touch (on the side or diagonally) \n <br>" +
                    "You have the right to move the pawn of your choice in one of the 8 directions \n <br>" +
                    "The Zen pawn (0) can be moved by the 2 players \n <br>" +
                    "You can eat an opponent's pawn if you land on his square \n <br>" +
                    "A move is made according to the number of pawns on the pawn line \n <br>" +
                    "For example if I want to move my pawn 3 up, and on the column of my pawn there are in total 3 pawns \n <br>" +
                    "Then my pawn will move up 3 spaces \n <br>" +
                    "The Zen pawn can only move if it comes close to another pawn. \n <br>" +
                    "More information in the user manual. \n <br>";
            soloOrDuo = "Do you want to play 2 players?";
            guiBackMenu = "Return to the main menu";
            guiTurn = "It's the turn of";
            guiQuickRules1 = "Choose the number of a pawn";
            guiQuickRules2 = "Then select the direction";
            guiQuickRules3 = "And if that respects the rules,";
            guiQuickRules4 = "then the pawn will be moved";
            guiStartGame = "Start a game";
            guiSeeRules = "See the rules";
            guiQuitHard = "Quit without saving";
            guiQuitWithSaving = "Quit with saving";
            newGame = "New game";
            lang = "Change to French";
            changeLang = "Changing language ..\n" +
                    "Done !";
        }
    }
}
