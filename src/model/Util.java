package model;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Scanner;

public class Util {

    public static String getStatusString(int move)
    {
        String str = null;
        if(move == -2){
            str = Language.badMove;
        } else if(move == -1){
            str = Language.badPawn;
        } else {
            str = Language.goodMove;
        }
        return str;
    }

    public static JButton StylishButton(String text){
        JButton btn = new JButton(text);
        btn.setHorizontalTextPosition(JButton.CENTER);
        btn.setVerticalTextPosition(JButton.CENTER);
        btn.setBorderPainted(false);
        btn.setFocusPainted(false);
        btn.setContentAreaFilled(false);
        btn.setIcon(new ImageIcon("data/button.png"));
        return btn;
    }

    public static JButton ControlButton(Direction dir){
        JButton btn = new JButton();
        btn.setBorderPainted(false);
        btn.setContentAreaFilled(false);
        btn.setIcon(new ImageIcon("data/controlButton"+dir.name()+".png"));
        return btn;
    }

    public static void writeZSAVEFile(String str) throws FileNotFoundException {
        PrintWriter writer = new PrintWriter("save.ZSAVE");
        writer.println(str);
        writer.close();
    }

    public static String readZSAVEFile() {
        StringBuilder strBuilder = new StringBuilder();
        Scanner in;

        try {
            in = new Scanner(new FileReader("save.ZSAVE"));

            while (in.hasNext()) {
                strBuilder.append(in.next());
            }
            in.close();
        } catch (FileNotFoundException e) {
            System.out.println("Erreur : fichier introuvable, vous avez peut être supprimé ou déplacé ?");
        }
        return strBuilder.toString();
    }

    public static HashMap<Direction,Direction> getOppositeDir(){
        HashMap<Direction,Direction> oppositeDir = new HashMap<>();
        oppositeDir.put(Direction.NW,Direction.SE);
        oppositeDir.put(Direction.N,Direction.S);
        oppositeDir.put(Direction.W,Direction.E);
        oppositeDir.put(Direction.SE,Direction.NW);
        oppositeDir.put(Direction.NE,Direction.SW);
        oppositeDir.put(Direction.S,Direction.N);
        oppositeDir.put(Direction.E,Direction.W);
        oppositeDir.put(Direction.SW,Direction.NE);
        return oppositeDir;
    }
}
