package model;
import java.util.ArrayList;

/**
 * Model the game board
 * with a grid where the pawns are placed
 */
public class GameBoard {
    private final ArrayList<Pawn> pawns;
    private final ArrayList<Pawn> killedPawns;
    private final Pawn[][] grid;

    /**
     * GameBoard constructor
     * @param length the grid length
     * @param pawns
     */
    public GameBoard(int length, ArrayList<Pawn> pawns){
        this.pawns = pawns;
        this.killedPawns = new ArrayList<Pawn>();
        this.grid = new Pawn[length][length];
        setup();
    }

    /**
     * GameBoard constructor from a Sring of the grid
     * @param length grid length
     * @param gridStr grid String (with all the pawns numbers)
     * @param p1 Player 1
     * @param p2 Player 2
     * @param pz Player for Zen
     */
    public GameBoard(int length, String gridStr, Player p1, Player p2, Player pz){

        this.pawns = new ArrayList<>();
        this.killedPawns = new ArrayList<>();
        this.grid = new Pawn[length][length];
        int x = 0, y = 0, i = 0;
        boolean isPawn;
        while(i < gridStr.length()-1) {
            isPawn = false;
            Pawn p;
            int pawn = Character.getNumericValue(gridStr.charAt(i));
            String pawnNumber = "" + gridStr.charAt(i) + gridStr.charAt(i + 1);
            if(pawn < 3 && pawn >= 0){
                isPawn = true;
                int n = (pawn==0)?Character.getNumericValue(gridStr.charAt(i+1)):Integer.parseInt(pawnNumber);

                if(n==0){
                    p = new Pawn(n, 5, 5, pz);
                } else {
                    p = new Pawn(n, y, x, (n < 13) ? p1 : p2);
                }
                this.pawns.add(p);
                this.grid[y][x] = p;

            } else if(gridStr.charAt(i) == ']'){
                x = -1;
                y++;
            } else if(gridStr.charAt(i) == '/'){
                p = null;
                this.grid[y][x] = p;
            }
            x++;
            if(isPawn) i+=2; else i++;
        }
    }

    /**
     * allows to set up the game board
     */
    private void setup() {
        int x, y;
        for (int i = 0; i < this.pawns.size() ; i++) {
            Pawn p = this.pawns.get(i);
            x = p.getPosition()[0];
            y = p.getPosition()[1];
            this.grid[x][y] = p;
        }
    }

    /**
     * allows to update the grid
     */
    public void update(char mode){
        for (Pawn p : this.pawns) {
            if(p.isMoving()){
                this.grid[p.getPosition()[0]][p.getPosition()[1]] = p;
                this.grid[p.getPosBefore()[0]][p.getPosBefore()[1]] = null;
                if(mode=='c') p.setMoved(false);
                break;
            }
        }
        if(mode=='c') {
            System.out.print("     ");
            for (int k = 0; k < this.getLength() - 1; k++) {
                System.out.print("  0" + k + "  ");
            }
            System.out.print("  10  ");
            System.out.print("\n____|");
            for (int k = 0; k < this.getLength(); k++) {
                System.out.print("______");
            }
            System.out.println();
            for (int i = 0; i < this.getLength(); i++) {
                System.out.print((i < 10) ? " 0" + i + " |" : " " + i + " |");
                for (int j = 0; j < this.getLength(); j++) {
                    Pawn p = this.grid[i][j];
                    if (p == null) {
                        System.out.print(" |__| ");
                    } else {
                        if (p.getNumber() < 10) {
                            System.out.print("  " + p.getColorStr() + " " + p + "\u001B[0m" + "  ");
                        } else {
                            System.out.print("  " + p.getColorStr() + p + "\u001B[0m" + "  ");
                        }
                    }
                }
                System.out.println("\n    |");
            }
        }
    }


    /**
     * to kill a pawn
     * @param p the pawn to kill
     */
    public void kill(Pawn p){
        if(p != null){
            this.killedPawns.add(p);
            this.pawns.remove(p);
            System.out.println(p + "is dead !");
        } else {
            System.out.println("Impossible de manger ce pion car il n'existe pas");
        }
    }

    /**
     * case getter
     * @param x line
     * @param y column
     * @return the pawn on this case (return null if there is nothing in this case)
     */
    public Pawn getCase(int x, int y){
        if(x >= this.getLength() || y >= this.getLength()) {
            System.out.println("GameBoard.getBox() : Error x or y out of board size");
            return new Pawn(1, 1, 1, null);
        } else return this.grid[x][y];
    }

    /**
     * allows to get a pawn by this coordinates
     * @param x x coord
     * @param y y coord
     * @return the pawn or null
     */
    public Pawn getPawn(int x, int y){
        Pawn p = null;
        for (Pawn pawn : this.pawns) {
            if(pawn.getPosition()[0] == x && pawn.getPosition()[1] == y){
                p = pawn;
            }
        }
        return p;
    }

    /**
     * allows to get a pawn by this number
     * @param i pawn number
     * @return the pawn or null
     */
    public Pawn getPawn(int i){
        Pawn p = null;
        for (Pawn pawn : this.pawns) {
            if(pawn.getNumber() == i){
                p = pawn;
            }
        }
        return p;
    }


    /**
     * pawns list getter
     * @param p the Player to see the pawns (if p is null : return all the pawns)
     * @return the pawns arraylist of a player (or all the pawns)
     */
    public ArrayList<Pawn> getPawnsList(Player p) {
        if (p == null) return this.pawns;
        ArrayList<Pawn> pawnsOfThisPlayer = new ArrayList<Pawn>();
        for (Pawn pawn : this.pawns) {
            if(pawn.getPlayer() == p){
                pawnsOfThisPlayer.add(pawn);
            }
        }
        update('c');

        return pawnsOfThisPlayer;
    }

    /**
     * @return the length of the grid
     */
    public int getLength(){
        return this.grid.length;
    }


    /**
     * this method counts the number of pawns in the column
     * @param col column index
     * @return the number of pawn in this column
     */
    public int countColPawn(int col){
        int nbPawns = 0;
        for (int i = 0; i < this.getLength() ; i++) {
            if(this.grid[i][col] != null){
                nbPawns++;
            }
        }
        return nbPawns;
    }

    /**
     * this method counts the number of pawns in the row
     * @param row row index
     * @return the number of pawn in this row
     */
    public int countRowPawn(int row){
        int nbPawns = 0;
        for (int i = 0; i < this.getLength() ; i++) {
            if(this.grid[row][i] != null){
                nbPawns++;
            }
        }
        return nbPawns;
    }

    /**
     * count the number of pawn in the diagonol
     * @param p pawn
     * @return pawn number in this diagonal
     */
    public int countDiagLeftPawn(Pawn p){
        int nbPawns = 0;
        int x = p.getPosition()[0];
        int y = p.getPosition()[1];
        while(x>=0 && y>=0 && x<this.getLength() && y<this.getLength()){
            if(this.grid[x][y] != null){
                nbPawns++;
            }
            x--;
            y--;
        }

        x = p.getPosition()[0]+1;
        y = p.getPosition()[1]+1;
        while(x>=0 && y>=0 && x<this.getLength() && y<this.getLength()){
            if(this.grid[x][y] != null){
                nbPawns++;
            }
            x++;
            y++;
        }
        return nbPawns;
    }

    /**
     * count the number of pawn in the diagonol
     * @param p pawn
     * @return pawn number in this diagonal
     */
    public int countDiagRightPawn(Pawn p){
        int nbPawns = 0;
        int x = p.getPosition()[0];
        int y = p.getPosition()[1];

        while(x >= 0 && y >= 0 && x < this.getLength() && y < this.getLength()){
            if(this.grid[x][y] != null){
                nbPawns++;
            }
            x++;
            y--;
        }

        x = p.getPosition()[0]-1;
        y = p.getPosition()[1]+1;
        while(x>=0 && y>=0 && x<this.getLength() && y<this.getLength()){
            if(this.grid[x][y] != null){
                nbPawns++;
            }
            x--;
            y++;
        }
        return nbPawns;
    }

    /**
     * @return the grid in String
     */
    public String getGridString() {
        StringBuilder gridBuilder = new StringBuilder();
        for (int i = 0; i < getLength(); i++) {
            for (int j = 0; j < getLength(); j++) {
                if(this.grid[i][j]==null){
                    gridBuilder.append("/");
                } else {
                    int n = this.grid[i][j].getNumber();
                    gridBuilder.append((n>9)?n:"0"+n);
                }
            }
            gridBuilder.append("]\n");
        }

        return gridBuilder.toString();
    }

}
