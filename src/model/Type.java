package model;
/**
 * Pawn Type enumeration
 * a pawn can be ZEN, WHITE or BLACK
 * there is 1 zen pawn and many white and black pawns
 */
public enum Type {
    ZEN,
    WHITE,
    BLACK
}
