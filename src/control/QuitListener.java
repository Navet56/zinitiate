package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This basic listener allows to quit the program
 */
public class QuitListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        System.exit(0);
    }
}
