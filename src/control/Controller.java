package control;
import view.*;
import model.*;
import view.graphicalView.*;

import javax.swing.*;
import java.sql.SQLOutput;


/**
 * Main Controller class
 */
public class Controller {
    private View view ;
    private Model model;

    /**
     * Controller constructor
     * This constructor creates a new View instance
     * @param model a generic Model
     */
    public Controller(Model model) {
        if (model != null){
            this.model = model;
            this.view = new View(model);
        }
        else System.out.println("Controller() : model is null");
    }

    /**
     * This method initializes the Game Panel
     */
    private void showGamePanel(boolean newGame){
        changeScreen(this.view.getMainFrame().getMainPanel());
        this.model.initGame(newGame, 'g');
        this.view.initBoard();
    }



    /**
     * This method allows to change the screen in the main frame (change the JPanel)
     * @param nextPanel the new panel to show
     */
    private void changeScreen(JPanel nextPanel){
        this.view.getMainFrame().getContentPane().removeAll();
        this.view.getMainFrame().setContentPane(nextPanel);
        this.view.getMainFrame().invalidate();
        this.view.getMainFrame().revalidate();

    }

    /**
     * Allows to show the CLI version of the game
     */
    public void showCLI(){
        this.view.startCLI();
    }

    /**
     * allows to move a pawn (in the GUI mode)
     * @param dir direction of the movement
     */
    private void movePawnGUI(Direction dir){
        if(this.model.getGame().getMode() == Mode.SOLO) {
            String numString = this.view.getMainFrame().getMainPanel().pawnSelectField.getText();
            int num = Integer.parseInt(numString);
            int move = this.model.getGame().movePawn(num, dir,true);
            this.view.getMainFrame().getMainPanel().getStatusLabel().setText(Util.getStatusString(move));
            this.updateGUI();
            int count = 0;
            while (!this.model.getGame().movePawnIA() && count < 10) {
                count++;
            }
            this.updateGUI();

            if(this.model.getGame().checkWin() && this.model.getGame().getCurrentPlayer().isAutoPlayer()) changeScreen(this.view.getMainFrame().getLosePanel());

        } else {
            String numString = this.view.getMainFrame().getMainPanel().pawnSelectField.getText();
            int num = Integer.parseInt(numString);
            int move = this.model.getGame().movePawn(num, dir, true);
            this.view.getMainFrame().getMainPanel().getStatusLabel().setText("<html>" + Util.getStatusString(move) + "</html>");
            this.updateGUI();
        }
    }

    /**
     * allows to update the grid
     */
    private void updateGUI(){
      this.model.getGame().getBoard().update('g');
      this.view.updatePlacementGUI();
      this.view.getMainFrame().getMainPanel().getPlayerLabel().setText(Language.guiTurn + this.model.getGame().getCurrentPlayer().getName());
    }

    /**
     * This method allows to change the language (in the GUI mode)
     * /!\ this method restart the game and so the old game is delete
     */
    private void changeLanguageGUI(){
        Language.setLanguage((Language.language=='f')?'a':'f');
        restartGUI();
    }

    /**
     * allows to show the game GUI and set the listeners
     */
    public void showGUI() {
        this.view.startGUI();
        //Menu's buttons listeners
        MainMenu mainMenu = this.view.getMainFrame().getMenu();
        mainMenu.getQuit().addActionListener(new QuitListener());
        mainMenu.getQuitSave().addActionListener(e -> quitSave());
        mainMenu.getNewFile().addActionListener(e -> restartGUI());
        mainMenu.getLanguage().addActionListener(e -> changeLanguageGUI());
        mainMenu.getRules().addActionListener(e -> new SimpleFrame(Language.rulesItem,Language.rules,900,300));
        mainMenu.getAbout().addActionListener(e -> new SimpleFrame("About Zen", "<html>" + Model.TITLE + " by " + Model.AUTHOR + "<br> Version "+ Model.VERSION + "<br>" + Model.LICENSE, 400, 150));
        mainMenu.getMusic().addActionListener(e -> this.view.playStopMusic());
        EndPanel winPanel = this.view.getMainFrame().getWinPanel();
        EndPanel losePanel =this.view.getMainFrame().getLosePanel();


        // GamePanel listeners
        GamePanel gamePanel = this.view.getMainFrame().getMainPanel();
        MenuPanel menuPanel =  this.view.getMainFrame().getMenuPanel();
        gamePanel.moveBtnE.addActionListener(e -> this.movePawnGUI(Direction.SW));
        gamePanel.moveBtnF.addActionListener(e -> this.movePawnGUI(Direction.SE));
        gamePanel.moveBtnH.addActionListener(e -> this.movePawnGUI(Direction.N));
        gamePanel.moveBtnD.addActionListener(e -> this.movePawnGUI(Direction.E));
        gamePanel.moveBtnG.addActionListener(e -> this.movePawnGUI(Direction.W));
        gamePanel.moveBtnB.addActionListener(e -> this.movePawnGUI(Direction.S));
        gamePanel.moveBtnA.addActionListener(e -> this.movePawnGUI(Direction.NE));
        gamePanel.moveBtnC.addActionListener(e -> this.movePawnGUI(Direction.NW));
        gamePanel.verifBtn.addActionListener(e -> winButtonListener());

        winPanel.getReturnButton().addActionListener(e -> restartGUI());
        losePanel.getReturnButton().addActionListener(e -> restartGUI());

        menuPanel.getNewGameButton().addActionListener(e -> showGamePanel(true));
        menuPanel.getLoadGameButton().addActionListener(e -> showGamePanel(false));
        menuPanel.getQuitButton().addActionListener(new QuitListener());
        menuPanel.getRulesButton().addActionListener(e -> new SimpleFrame(Language.rulesItem,Language.rules,900,300));
        menuPanel.getStartButton().addActionListener(e -> menuPanel.goToGameChoice());

    }

    /**
     * This method save the game and then restart the program
     */
    private void quitSave() {
        this.model.saveGame();
        SwingUtilities.invokeLater(()-> new SimpleFrame("Notification", ""+Language.wellSaved, 700, 50));
        restartGUI();

    }

    /**
     * This method allows to restart the program (in the GUI mode)
     */
    private void restartGUI() {
        this.view.getMainFrame().dispose();
        this.view.stopMusic();
        this.view = new View(this.model);
        showGUI();
        this.view.getMainFrame().update();

    }

    /**
     * Allows to check if the current player won
     * if yes, this method shows the win screen,
     * else print that the game continue in the status label
     */
    private void winButtonListener(){
        if(this.model.getGame().checkWin()) {
            changeScreen(this.view.getMainFrame().getWinPanel());
        } else {
            this.view.getMainFrame().getMainPanel().getStatusLabel().setText(Language.notWon);
        }
    }
}
