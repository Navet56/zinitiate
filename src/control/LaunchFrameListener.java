package control;

import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A generic listener to launch a new frame
 */
public class LaunchFrameListener implements ActionListener {

    private JFrame frame;

    public LaunchFrameListener(){
    }

    /**
     * If this method is called, the frame appears
     * @param e
     */
    public void actionPerformed(ActionEvent e) {

    }
}
