 ZEN L'INITIÉ 
 
 Par Evan DIBERDER

  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  
  @@@@@@@@@@@@@@@@@@@@@@@@*                         *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  
  @@@@@@@@@@@@@@@@@@*          *@@@@@@@@@@@@@@*              @@@@@@@@@@@@@@@@@@@@@  
  @@@@@@@@@@@@@@@   ,@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@          @@@@@@@@@@@@@@@@@@  
  @@@@@@@@@@@@    @@@@@@@@@@@@@@@                   @@@@@@@@        @@@@@@@@@@@@@@  
  @@@@@@@@@@   .@@@@@@@@@         @@@@@@@@@@@@@@@@@.    @@@@@@@@@    @@@@@@@@@@@@@  
  @@@@@@@@@   @@@@@@@     @@@@@@@@@@@@@@@@@@@@@@     @@@@@@@@@@@@@@@    @@@@@@@@@@  
  @@@@@@@@   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    %@@@@@@@@@@@@@@@@@@@@@   @@@@@@@@  
  @@@@@@   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@/    @@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@  
  @@@@@   @@@@@@@@@@@@@@@@@@@@@@@@@@@@.    @@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@   @@@@  
  @@@@   @@@@@@@@@@@@@@@@@@@@@@@@@@    %@@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@   @@@  
  @@@   @@@@@@@@@@@@@@@@@@@@@@@@@   /@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  %@@@@@   @@  
  @@@  @@@@@@@@@@@@@@@@@@@@@@@%   @@@@@@@@             @    @@@@@@@@@   @@@@@.  @@  
  @@   @@@@@@@@@@@@@@@@@@@     @@@@@@@@    @@@@@@@@@@@@       (@@@@@@   @@@@@   @@  
  @@   @@@@@@@@@@@@@@@@@@    @@@@@@@@@@  @@@@@@@@@@@@@@   @@@*   @@@@   @@@@@   @@  
  @@   @@@@@@@@@@@@@@@@       @@@@@@@@@@             @@   @@@@@@   @@@  @@@@@    @  
  @@@   @@@@@@@@@@@@@@      @@@@@@@@@@@@  @@@@@@@@@@@@@@   @@@@@@@@   @  &@@@   @@  
  @@@   @@@@@@@@@@@@      @@@@@@@@@@@@@@  /@@@@@@@@@@@@@   @@@@@@@@@@    @@@@   @@  
  @@@   @@@@@@@@@@      @@@@@@@@@@@@@@@               @   @@@@@@@@@@@@@@@@@@   @@@  
  @@@   @@@@@@@@        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  @@@@@@@@@@@@@@@@@   ,@@@  
  @@@@  @@@@@@@       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    @@@@  
  @@@@@   @@@@@                         @@@@@@@@@@@@  @@@@@@@@@ @@@@@@@@@@   @@@@@  
  @@@@@   @@@@@@@@@@@@@@@@@@@@@@@@  @@@@@@@@@@@@@@@@@@@@@@          @@@@@    @@@@@  
  @@@@@@   .@@@@@@@@   @@@@   *@@@  @@@   @         @@  @@@    @@@@@@@@@   @@@@@@@  
  @@@@@@@@   .@@@@@@@  .@@&      #   @@  @@@%   @@@@@   @@*         @@    @@@@@@@@  
  @@@@@@@@@@     @@@@   @@%  *@@    @@@  /@@@   @@@@@  #@@    @@@@%    &@@@@@@@@@@  
  @@@@@@@@@@@@@@    @@@@@@@&@@@@@@&@@@@   @@@  (@@@@@ (@@@@@.      #@@@@@@@@@@@@@@  
  @@@@@@@@@@@@@@@@@      (@@@@@@@@@@@@@@@@@@@@@@@@@@@@(      &@@@@@@@@@@@@@@@@@@@@  
  @@@@@@@@@@@@@@@@@@@@@@.                              .@@@@@@@@@@@@@@@@@@@@@@@@@@


Pour lancer le jeu en mode graphique
--------------------------------------------------------

Allez dans release et double cliquez sur Zen.jar

ou

cd release; java -jar Zen.jar

Pour lancer le jeu en mode console
-------------------------------------

cd release; java -jar Zen.jar -cli
